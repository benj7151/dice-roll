function rollOfDice(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}
var count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for (index = 0; index < 1000; index++) {
    let x = rollOfDice(1,6);
    let y = rollOfDice(1,6);
    let sum = x+y;
    count[sum]++
}
document.write(count);
var ctx = document.getElementById('barChart').getContext('2d');
var barChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
        datasets: [{
            label: "Dice Roll Results",
            backgroundColor: 'red',
            data: count
        }],

        options:{}
    }
})